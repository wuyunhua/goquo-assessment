import React, { Component, Fragment } from 'react'

import Layout from '../components/layout'
import '../../node_modules/react-bulma-components/dist/react-bulma-components.min.css'

import {
  Button, Columns, Footer, Tabs, Table, Level   
} from 'react-bulma-components';

import standard from '../images/standard.jpg'
import executive from '../images/executive.jpg'
import presidential from '../images/presidential.jpg'

import data from '../data.json'

class IndexPage extends Component {
  constructor(props) {
    super(props);

    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(data) {    
    document.getElementById('debug').textContent = "Booked " + data.roomTypeLabel + ", " + data.bedTypeLabel + ", " + data.boardCodeDescription + ", " + data.totalPrice;
  }

  renderTable() {   
    const room_types = ['Standard', 'Executive', 'Presidential'];      

    const room_descriptions = [
      'Indulge in the lavishness and comfort of your own personal space while being surrounded by lush greeneries which brings you closer to nature.',
      'A plush sanctuary of comfort with plenty of space to spread out, wake up to crisp fresh air and the sound of birds chirping as you start your day.',
      'Two charming and beautiful furnished rooms interconnecting to form a perfect abode for families to stay together while enjoying quality time together.'
    ];

    const room_images = [
      standard, executive, presidential
    ];

    let rows = [];

    room_types.forEach((room_type, index) => {
      let options = [];
      
      data.forEach((room, i) => {
        if (room_type === room.roomTypeLabel) {          
          options.push(
            <Level key={i}>
              <Level.Side align="left">
                <Level.Item>
                  <b>{room.bedTypeLabel}</b>-<i>{room.boardCodeDescription}</i>: <b>RM {room.totalPrice}</b>
                </Level.Item>
              </Level.Side>
              <Level.Side align="right">
                <Level.Item>
                  <Button color="success" onClick={e => this.handleClick(room)}>Book Room</Button>
                </Level.Item>
              </Level.Side>
            </Level>
          );
        }
      });

      rows.push(
        <tr key={index}>
          <td><img src={room_images[index]} alt=""></img></td>
          <td><b>{room_type}</b></td>
          <td>
            <p>{room_descriptions[index]}</p>
            <br />
            <b>Available Options:</b>
            {options}
          </td>
        </tr>
      );
    });

    return (
      <Fragment>
        {rows}
      </Fragment>
    );
  }

  render() {
    return (
      <Layout>
        <Tabs>
          <Tabs.Tab>1. Choose Date</Tabs.Tab>
          <Tabs.Tab active>2. Choose Room</Tabs.Tab>
          <Tabs.Tab>3. Confirmation</Tabs.Tab>
          <Tabs.Tab>4. Checkout</Tabs.Tab>
        </Tabs>
        <Columns>
          <Columns.Column>
            <Table>
              <thead>
                <tr>
                  <th>AVAILABLE ROOMS</th>
                </tr>
              </thead>
              <tbody>
                {
                  this.renderTable()
                }                
              </tbody>
            </Table>
          </Columns.Column>
        </Columns>
        <Footer>
          <p>Debug Logs: </p>
          <p id="debug"></p>
        </Footer>
      </Layout>
    );
  }
}

export default IndexPage
